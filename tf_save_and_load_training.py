import tensorflow as tf
import numpy as np
import tf2onnx
import onnx
import os
from onnx_tf.backend import prepare
from utils import tf_utils


with tf.Session() as sess:
    In = tf.placeholder(shape=[5, 1], name='input_ph', dtype=tf.float32)
    Target = tf.placeholder(shape=[5, 1], name='target_ph', dtype=tf.float32)

    top = tf.
    A = tf.constant(value=1.5, shape=[5, 1])
    B = tf.constant(value=1.5, shape=[5, 1])
    C = tf.add(A, B)
    D = tf.add(A, In)
    E = tf.add(A, In2, name='output')

    onnx_graph = tf2onnx.tfonnx.process_tf_graph(sess.graph, input_names=['input_ph:0', 'input_ph2:0'], output_names=["output:0"])
    model_proto = onnx_graph.make_model("tf_save")
    with open("model.onnx", "wb") as f:
        f.write(model_proto.SerializeToString())

    print(onnx_graph.outputs)
    ## IMPORTANT!! You should rename the ph names
    tf_utils.tf_rename('model.onnx', ['input_ph:0', 'input_ph2:0', 'output:0'])


## Load and run onnx file
sess = tf.Session()
input = np.full((5,1), 1.5)
input2 = np.full((5,1), 0.5)
print(sess.run(D, feed_dict={'input_ph:0':input, 'input_ph2:0':input2}))

onnx_model_path = 'model.onnx'
assert os.path.exists(onnx_model_path), "ONNX model is not found."
model = onnx.load(onnx_model_path) # Load the ONNX file
tf_rep = prepare(model) # Import the ONNX model to Tensorflow
print(tf_rep.inputs)
print(tf_rep.outputs)
print(input)
print(input2)
print(tf_rep.run([input, input2]))